#![no_std]

const A64_NOP: u32 = 0xd503201f;

use core::ptr::{null, null_mut};

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq)]
struct Patch {
    pub base: JitPointer,
    pub left_shift_count: u8,
    pub mask: u32,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct Insns<const A64_MAX_REFERENCES: usize> {
    pub instruction_ptr: u64,
    pub patches: [Patch; A64_MAX_REFERENCES],
}

impl<const A64_MAX_REFERENCES: usize> Default for Insns<A64_MAX_REFERENCES> {
    fn default() -> Self {
        Self {
            instruction_ptr: Default::default(),
            patches: [Default::default(); A64_MAX_REFERENCES],
        }
    }
}

struct HookContext<const A64_MAX_INSTRUCTIONS: usize, const A64_MAX_REFERENCES: usize> {
    pub base: usize,
    pub end: usize,
    pub data: [Insns<A64_MAX_REFERENCES>; A64_MAX_INSTRUCTIONS],
}

impl<const A64_MAX_INSTRUCTIONS: usize, const A64_MAX_REFERENCES: usize>
    HookContext<A64_MAX_INSTRUCTIONS, A64_MAX_REFERENCES>
{
    #[inline]
    pub fn is_in_fixing_range(&self, absolute: usize) -> bool {
        absolute >= self.base && absolute < self.end
    }

    #[inline]
    pub fn ins_index(&self, absolute: usize) -> usize {
        (absolute - self.base) / core::mem::size_of::<u32>()
    }

    #[inline]
    pub fn update_current_index(&mut self, inp: *const u32, outp: *const u32) -> usize {
        let index = self.ins_index(inp as usize);
        self.data[index].instruction_ptr = outp as u64;
        index
    }

    #[inline]
    pub fn reset_current_ins(&mut self, idx: usize, outp: *const u32) {
        self.data[idx].instruction_ptr = outp as u64;
    }

    pub fn insert_patch_map(
        &mut self,
        idx: usize,
        base: JitPointer,
        left_shift_count: Option<u8>,
        mask: Option<u32>,
    ) {
        let patch = self.data[idx]
            .patches
            .iter_mut()
            .find(|Patch { base, .. }| base.rw.is_null())
            .expect("Out of patches");

        *patch = Patch {
            base,
            left_shift_count: left_shift_count.unwrap_or(0),
            mask: mask.unwrap_or(u16::MAX as u32),
        };
    }

    pub fn process_patch_map(&mut self, idx: usize) {
        for patch in self.data[idx].patches.iter_mut() {
            if patch.base.rw.is_null() {
                break;
            }

            unsafe {
                let mut patched = (self.data[idx].instruction_ptr - patch.base.rx as u64) as u32;

                patched >>= 2;
                patched <<= patch.left_shift_count;
                patched &= patch.mask;

                *patch.base.rw |= patched;

                patch.base.rw = null_mut();
                patch.base.rx = null();
            }
        }
    }

    pub(crate) fn fix_branch_imm(
        &mut self,
        input: &mut JitPointer,
        output: &mut JitPointer,
    ) -> bool {
        const MASK_BITS: u32 = 6;
        const MASK: u32 = 0xfc000000;
        const RIGHT_MASK: u32 = 0x03ffffff;
        const OP_B: u32 = 0x14000000;
        const OP_BL: u32 = 0x94000000;

        let ins = unsafe { *input.rw };
        let opcode = ins & MASK;

        if opcode == OP_B || opcode == OP_BL {
            let idx = self.update_current_index(input.rx, output.rx);
            let absolute_addr =
                input.rx as usize + (((ins as i32) << MASK_BITS) >> (MASK_BITS - 2)) as usize;
            let mut new_pc_offset = (absolute_addr as isize - output.rx as isize) >> 2;
            let special_fix_type = self.is_in_fixing_range(absolute_addr);

            if !special_fix_type && new_pc_offset.unsigned_abs() >= (RIGHT_MASK >> 1) as usize {
                let b_aligned = (unsafe { output.rx.offset(2) } as usize) & 7 == 0;
                if opcode == OP_B {
                    if !b_aligned {
                        unsafe {
                            *output.rw = A64_NOP;
                        }
                        self.reset_current_ins(idx, output.rx);
                        output.increment(1);
                    }
                    unsafe {
                        *output.rw = 0x58000051;
                    } // LDR X17, #0x8
                } else {
                    if b_aligned {
                        unsafe {
                            *output.rw = A64_NOP;
                        }
                        self.reset_current_ins(idx, output.rx);
                        output.increment(1);
                    }
                    unsafe {
                        *output.rw = 0x58000071;
                    } // LDR X17, #0xC
                    output.increment(1);
                    unsafe {
                        *output.rw = 0x1000009e;
                    } // ADR X30, #16
                }
                output.increment(1);
                unsafe {
                    *output.rw = 0xd61f0220;
                } // BR X17
                output.increment(1);
                unsafe {
                    *(output.rw as *mut u64) = absolute_addr as u64;
                } // Absolute address
                output.increment(2);
            } else {
                if special_fix_type {
                    let ref_idx = self.ins_index(absolute_addr);
                    if ref_idx <= idx {
                        new_pc_offset =
                            (self.data[ref_idx].instruction_ptr - output.rx as u64) as isize;
                    } else {
                        self.insert_patch_map(ref_idx, *output, None, Some(RIGHT_MASK));
                        new_pc_offset = 0;
                    }
                }

                unsafe { *output.rw = opcode | (new_pc_offset as u32 & !MASK) }
                output.increment(1);
            }

            input.increment(1);
            self.process_patch_map(idx);

            true
        } else {
            false
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct JitPointer {
    pub rw: *mut u32,
    pub rx: *const u32,
}

impl JitPointer {
    pub fn new(rw: *mut u32, rx: *const u32) -> Self {
        Self { rw, rx }
    }

    pub fn increment(&mut self, offset: isize) {
        unsafe {
            self.rw = self.rw.offset(offset);
            self.rx = self.rx.offset(offset);
        }
    }
}

impl Default for JitPointer {
    fn default() -> Self {
        Self {
            rw: null_mut(),
            rx: null(),
        }
    }
}

pub trait HookHelper<const A64_MAX_INSTRUCTIONS: usize, const A64_MAX_REFERENCES: usize> {
    /// Expectation: allocate `size` memory, return a RW pointer and X pointer (in case of RWX pages these will be the same reference)
    fn allocate_jit(&mut self, size: usize) -> Result<JitPointer, &'static str>;
    /// Expectation: flush all previously allocated JIT memory
    fn flush_jit(&self);
    /// Unrecoverable error, panic
    fn handle_error(&self, err: &'static str) -> !;
}

impl<const A64_MAX_INSTRUCTIONS: usize, const A64_MAX_REFERENCES: usize>
    dyn HookHelper<A64_MAX_INSTRUCTIONS, A64_MAX_REFERENCES>
{
    fn ok_or_panic<T>(&self, res: Result<T, &'static str>) -> T {
        match res {
            Ok(t) => t,
            Err(why) => self.handle_error(why),
        }
    }

    fn fix_instructions_trampoline(
        &mut self,
        count: isize,
        mut input: JitPointer,
        mut output: JitPointer,
    ) -> Result<(), &'static str> {
        let mut context = HookContext {
            base: input.rx as usize,
            end: unsafe { input.rx.offset(count) } as usize,
            data: [Insns::<A64_MAX_REFERENCES>::default(); A64_MAX_INSTRUCTIONS],
        };

        for _ in 0..count {
            if context.fix_branch_imm(&mut input, &mut output) {
                continue;
            }
            
            let idx = context.update_current_index(input.rx, output.rx);
            context.process_patch_map(idx);

            unsafe {
                *output.rw = *input.rw;
                input.increment(1);
                output.increment(1);
            }
        }

        const OFFSET_MASK: u32 = 0x03ffffff;
        let callback = input.rx;
        let pc_offset = (callback as isize - output.rx as isize) >> 2;
        if pc_offset.unsigned_abs() >= (OFFSET_MASK as usize >> 1) {
            // Absolute
            if (unsafe { output.rx.offset(2) } as u64) & 7 != 0 {
                unsafe { *output.rw = A64_NOP }
                output.increment(1);
            }

            unsafe {
                *output.rw = 0x58000051;
            } // LDR X17, #0x8
            output.increment(1);
            unsafe {
                *output.rw = 0xd61f0220;
            } // BR X17
            output.increment(1);
            unsafe {
                *(output.rw as *mut u64) = callback as u64;
            } // Callback address
            output.increment(2);
        } else {
            // Relative
            unsafe { *output.rw = 0x14000000 | ((pc_offset as u32) & OFFSET_MASK) }; // "B" ADDR_PCREL26
            output.increment(1);
        }

        Ok(())
    }

    fn hook_function_raw_internal(
        &mut self,
        mut address: JitPointer,
        replacement: *const u32,
    ) -> Result<*const u32, &'static str> {
        const OFFSET_MASK: u64 = 0x03ffffff;
        let pc_offset = (replacement as isize - address.rx as isize) >> 2;

        if pc_offset.unsigned_abs() < (OFFSET_MASK as usize >> 1) {
            let trampoline = self.allocate_jit(10)?;

            self.fix_instructions_trampoline(1, address, trampoline)?;

            Ok(trampoline)
        } else {
            let count = 4; //if ((unsafe { address.rx.add(2) } as usize) & 7) != 0 { 5 } else { 4 };
            let trampoline = self.allocate_jit(count * 10)?;

            self.fix_instructions_trampoline(count as isize, address, trampoline)?;

            if count == 5 {
                unsafe { *address.rw = A64_NOP };
                address.increment(1);
            }

            unsafe { *address.rw = 0x58000051 };
            address.increment(1);
            unsafe { *address.rw = 0xd61f0220 };
            address.increment(1);
            unsafe {
                *(address.rw as *mut usize) = replacement as usize;
            } // Callback address
            address
                .increment((core::mem::size_of::<usize>() / core::mem::size_of::<u32>()) as isize);

            Ok(trampoline)
        }
        .map(|JitPointer { rx: x, .. }| x)
    }

    pub fn hook_function_raw(
        &mut self,
        address: JitPointer,
        replacement: *const u32,
    ) -> *const u32 {
        let res = self.hook_function_raw_internal(address, replacement);
        self.ok_or_panic(res)
    }
}
