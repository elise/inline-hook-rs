use inlinehook_rs::{HookHelper, JitPointer};

// main:
//         stp     x29, x30, [sp, -16]!
//         mov     x29, sp
//         bl      to_be_hooked
//         ldp     x29, x30, [sp], 16
//         ret
// to_be_hooked: /* #0x14 */
//         sub     sp, sp, #16
//         str     wzr, [sp, 12]
//         str     wzr, [sp, 8]
//         b       .L4
// .L6:
//         ldr     w2, [sp, 8]
//         mov     w0, 43691
//         movk    w0, 0x2aaa, lsl 16
//         smull   x0, w2, w0
//         lsr     x0, x0, 32
//         asr     w1, w0, 1
//         asr     w0, w2, 31
//         sub     w1, w1, w0
//         mov     w0, w1
//         lsl     w0, w0, 1
//         add     w0, w0, w1
//         lsl     w0, w0, 2
//         sub     w1, w2, w0
//         cmp     w1, 0
//         beq     .L5
//         ldr     w0, [sp, 12]
//         add     w0, w0, 1
//         str     w0, [sp, 12]
// .L5:
//         ldr     w0, [sp, 8]
//         add     w0, w0, 1
//         str     w0, [sp, 8]
// .L4:
//         ldr     w0, [sp, 8]
//         cmp     w0, 122
//         ble     .L6
//         ldr     w0, [sp, 12]
//         add     sp, sp, 16
//         ret
static PAYLOAD: &[u8] = b"\xfd\x7b\xbf\xa9\xfd\x03\x00\x91\x03\x00\x00\x94\xfd\x7b\xc1\xa8\xc0\x03\x5f\xd6\xff\x43\x00\xd1\xff\x0f\x00\xb9\xff\x0b\x00\xb9\x16\x00\x00\x14\xe2\x0b\x40\xb9\x60\x55\x95\x52\x40\x55\xa5\x72\x40\x7c\x20\x9b\x00\xfc\x60\xd3\x01\x7c\x01\x13\x40\x7c\x1f\x13\x21\x00\x00\x4b\xe0\x03\x01\x2a\x00\x78\x1f\x53\x00\x00\x01\x0b\x00\x74\x1e\x53\x41\x00\x00\x4b\x3f\x00\x00\x71\x80\x00\x00\x54\xe0\x0f\x40\xb9\x00\x04\x00\x11\xe0\x0f\x00\xb9\xe0\x0b\x40\xb9\x00\x04\x00\x11\xe0\x0b\x00\xb9\xe0\x0b\x40\xb9\x1f\xe8\x01\x71\x2d\xfd\xff\x54\xe0\x0f\x40\xb9\xff\x43\x00\x91\xc0\x03\x5f\xd6";
static HOOK_OFFSET: usize = 0x14;

pub extern "C" fn replacement() -> i32 {
    666
}

pub struct RWXHook {
    pub memory: Vec<u8>,
    pub offset: usize,
}

impl RWXHook {
    pub fn new(input: &[u8]) -> Self {
        let mut size = input.len();
        size += size % 4;

        let mut memory = vec![0; input.len() + 0x1000];

        memory[..input.len()].clone_from_slice(input);

        Self {
            memory,
            offset: size,
        }
    }

    pub fn finish(mut self) -> Vec<u8> {
        self.memory.truncate(self.offset);
        self.memory
    }
}

impl HookHelper<5, 10> for RWXHook {
    fn allocate_jit(&mut self, size: usize) -> Result<inlinehook_rs::JitPointer, &'static str> {
        let size = size + (size % 4);

        if self.offset + size > 0x1000 {
            return Err("Mew! Out of space");
        }

        let base = unsafe { self.memory.as_mut_ptr().add(self.offset) };

        self.offset += size;

        Ok(JitPointer {
            rw: base as _,
            rx: base as _,
        })
    }

    fn flush_jit(&self) {}

    fn handle_error(&self, err: &'static str) -> ! {
        panic!("Err: {}", err);
    }
}

/// This takes a static JIT payload and hooks a routine in it with a runtime rust function.
fn main() {
    let mut hook = RWXHook::new(PAYLOAD);

    let address = unsafe {
        JitPointer {
            rw: hook.memory.as_mut_ptr().add(HOOK_OFFSET) as _,
            rx: hook.memory.as_ptr().add(HOOK_OFFSET) as _,
        }
    };
    (&mut hook as &mut dyn HookHelper<5, 10>).hook_function_raw(address, replacement as _);

    let hooked = hook.finish();

    for x in &hooked {
        print!("\\x{:02x}", x)
    }
    println!();
}
